const express = require('express');
const router = express.Router();
const { GetPromoCodes, GetPromoCodeById, AddPromoCode, UpdatePromoCode, DeletePromoCode } = require('../controllers/PromoCodeController');



router.get('/', GetPromoCodes)

router.get('/id/:ProductId', GetPromoCodeById)

router.post('/', AddPromoCode)

router.put('/update/:ProductId', UpdatePromoCode)

router.delete('/delete/:ProductId', DeletePromoCode)




module.exports = router