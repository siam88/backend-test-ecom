const express = require('express');
const router = express.Router();
const Orders = require('../models/orders');
const { GetOrders, GetOrdersById, AddOrders, UpdateOrders, DeleteOrders } = require('../controllers/OrderController');



router.get('/', GetOrders)

router.get('/id/:ProductId', GetOrdersById)

router.post('/', AddOrders)

router.put('/update/:ProductId', UpdateOrders)

router.delete('/delete/:ProductId', DeleteOrders)




module.exports = router