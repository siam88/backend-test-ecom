const express = require('express');
const router = express.Router();
const Products = require('../models/products');
const multer = require('multer');
const path = require('path');
const { GetProducts, GetProductById, AddProduct, UpdateProduct, DeleteProduct } = require('../controllers/ProductController');


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "_" + file.originalname)
    }
})
var upload = multer({
    storage: storage,
    limits: {
        fieldSize: 500 * 500
    }
})

router.get('/', GetProducts)

router.get('/id/:ProductId', GetProductById)

router.post('/', upload.single('image'), AddProduct)

router.put('/update/:ProductId', UpdateProduct)

router.delete('/delete/:ProductId', DeleteProduct)




module.exports = router