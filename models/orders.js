const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

    itemPrice: {
        type: Number,
        required: true
    },

    status: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: {
        type: Date
    }
},
    {
        timestamps: true
    })

module.exports = mongoose.model('Orders', orderSchema)