const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
    },
    color: {
        type: String,
        required: true
    },
    size: {
        type: String,
        required: true
    },
    shippingMethod: {
        type: String,
        required: true
    },
    ProductPrice: {
        type: Number,
        required: true
    },
    discountRate: {
        type: Number

    },
    shippingCharge: {
        type: Number,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: {
        type: Date
    }
},
    {
        timestamps: true
    })

module.exports = mongoose.model('Products', productSchema)