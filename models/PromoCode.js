const mongoose = require('mongoose');

const promoCodeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },

    usable: {
        type: Number,
        required: true
    },
    discountRate: {
        type: Number,
        required: true
    },

    active: {
        type: Boolean,
        default: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    updatedAt: {
        type: Date
    }
},
    {
        timestamps: true
    })

module.exports = mongoose.model('PromoCodes', promoCodeSchema)