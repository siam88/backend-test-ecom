const mongoose = require('mongoose');


module.exports = function () {
    mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
    const db = mongoose.connection
    db.on('error', (error) => console.log(error))
    db.once('open', (error) => console.log("connected to database"))
}