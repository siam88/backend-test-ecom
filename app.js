require('dotenv').config();
var bodyParser = require("body-parser");
const productRouter = require('./routes/products');
const OrderRouter = require('./routes/Order')
const PromoCodeRouter = require('./routes/PromoCode')

const express = require('express');
const router = require('./routes/products');

const app = express();

app.use('/uploads', express.static("uploads"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


//database connection
require("./public/DBCon")();

//to use json file
app.use(express.json())

//routes
app.use('/products', productRouter)
app.use('/orders', OrderRouter)
app.use('/promoCodes', PromoCodeRouter)


//we start listening to the server
const port = process.env.PORT || 3002;
app.listen(port, () => console.log(`Listening on post ${port}...`));