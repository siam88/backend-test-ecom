const PromoCodes = require('../models/PromoCode');
const _ = require("lodash");


/**
 * Add product
 */
exports.AddPromoCode = async (req, res) => {
    const promoCodes = new PromoCodes(req.body)
    try {
        const newPromoCode = await promoCodes.save()
        res.status(201).json(newPromoCode)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
}

/**
 * get All product info
 */
exports.GetPromoCodes = async (req, res) => {
    try {
        const promoCodes = await PromoCodes.find();
        res.json(promoCodes)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
}
/**
 * get product by id
 */
exports.GetPromoCodeById = async (req, res) => {
    try {
        const promoCode = await PromoCodes.findById(req.params.ProductId)
        return res.send(promoCode)
    } catch (err) {
        return res.status(404).send(err);
    }
}
/**
 * update product
 */
exports.UpdatePromoCode = async (req, res) => {
    try {
        let promoCode = await PromoCodes.findByIdAndUpdate(req.params.ProductId, {
            $set: req.body
        })
        res.status(200).send(promoCode);

    } catch (error) {
        return res.status(404).send(error);
    }
}

/**
 * delete Product
 */
exports.DeletePromoCode = async (req, res) => {
    try {
        let promoCodes = await PromoCodes.findById(req.params.ProductId);
        if (promoCodes == null) {
            return res.status(404).json({ message: 'Cannot find product' })
        }
        await promoCodes.remove()
        res.json({ message: 'Product Deleted ' })
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }
}


