const Orders = require('../models/orders');
const _ = require("lodash");


/**
 * Add Orders
 */
exports.AddOrders = async (req, res) => {
    const orders = new Orders(req.body)
    try {
        const newOrders = await orders.save()
        res.status(201).json(newOrders)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
}
/**
 * get All product info
 */
exports.GetOrders = async (req, res) => {
    try {
        const orders = await Orders.find();
        res.json(orders)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
}
/**
 * get product by id
 */
exports.GetOrdersById = async (req, res) => {
    try {
        const orders = await Orders.findById(req.params.ProductId)
        return res.send(orders)
    } catch (err) {
        return res.status(404).send(err);
    }
}
/**
 * update product
 */
exports.UpdateOrders = async (req, res) => {
    try {
        let order = await Orders.findByIdAndUpdate(req.params.ProductId, {
            $set: req.body
        })
        res.status(200).send(order);

    } catch (error) {
        return res.status(404).send(error);
    }
}

/**
 * delete Product
 */
exports.DeleteOrders = async (req, res) => {
    try {
        let orders = await Orders.findById(req.params.ProductId);
        if (orders == null) {
            return res.status(404).json({ message: 'Cannot find product' })
        }
        await orders.remove()
        res.json({ message: 'Product Deleted ' })
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }
}


