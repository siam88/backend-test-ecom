const Products = require('../models/products');
const _ = require("lodash");


/**
 * Add product
 */
exports.AddProduct = async (req, res) => {

    if (req.file.size > 500 * 500) {
        return res.status(400).json({ message: "picture size should be  500*500 " })
    }

    const products = new Products({
        name: req.body.name,
        image: req.file.path,
        color: req.body.color,
        size: req.body.size,
        shippingMethod: req.body.shippingMethod,
        ProductPrice: req.body.ProductPrice,
        discountRate: req.body.discountRate,
        shippingCharge: req.body.shippingCharge,
        active: req.body.active,

    })
    try {
        const newProducts = await products.save()
        res.status(201).json(newProducts)
    } catch (err) {
        res.status(400).json({ message: err.message })
    }
}

/**
 * get All product info
 */
exports.GetProducts = async (req, res) => {
    try {
        const products = await Products.find();
        res.json(products)
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
}
/**
 * get product by id
 */
exports.GetProductById = async (req, res) => {
    try {
        const product = await Products.findById(req.params.ProductId)
        return res.send(product)
    } catch (err) {
        return res.status(404).send(err);
    }
}
/**
 * update product
 */
exports.UpdateProduct = async (req, res) => {
    try {
        let product = await Products.findByIdAndUpdate(req.params.ProductId, {
            $set: req.body
        })
        res.status(200).send(product);

    } catch (error) {
        return res.status(404).send(error);
    }
}

/**
 * delete Product
 */
exports.DeleteProduct = async (req, res) => {
    try {
        let products = await Products.findById(req.params.ProductId);
        if (products == null) {
            return res.status(404).json({ message: 'Cannot find product' })
        }
        await products.remove()
        res.json({ message: 'Product Deleted ' })
    } catch (err) {
        return res.status(500).json({ message: err.message })
    }
}


